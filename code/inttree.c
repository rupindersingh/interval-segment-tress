#include<stdio.h> 
#include<stdbool.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
typedef long long lld;
typedef struct pair
{
	int xlo;
	int xhi;
}PAIR;

typedef struct tree
{
	struct tree*left;
	struct tree*right;
	int xmed;
	PAIR mr[10000];
	PAIR ml[10000];
	int numl,numr;
}TREE;

int compml(const void*p1,const void *p2)
{
	return ((PAIR)(*((PAIR*)p1))).xlo-((PAIR)(*((PAIR*)p2))).xlo;
}
int compmr(const void*p1,const void *p2)
{
	return ((PAIR)(*((PAIR*)p2))).xhi-((PAIR)(*((PAIR*)p1))).xhi;
}
int comps(const void*p1,const void *p2)
{
	return ((int)(*((int*)p1)))-((int)(*((int*)p2)));
}

TREE* intTreeNode(PAIR s[],int left,int right)
{
	if ((right-left)<0)
		return NULL;
	TREE * newnode=malloc(sizeof(TREE));
	int a[10000],cnt=-1,i;
	for(i=left;i<=right;i++)
	{
		a[++cnt]=s[i].xlo;
		a[++cnt]=s[i].xhi;
	}
	qsort(a,cnt+1,sizeof(int),comps);
	int xmed=a[cnt/2];
	newnode->xmed=xmed;
	PAIR l[1000],r[1000],m[1000];
	int cntr=-1,cntm=-1,cntl=-1;
	cnt=-1;
	for(i=left;i<=right;i++)
	{
		if(s[i].xhi<xmed)
			l[++cntl]=s[i];
		if(s[i].xlo>xmed)
			r[++cntr]=s[i];
		if(s[i].xlo<=xmed&&s[i].xhi>=xmed)
		{
			m[++cntm]=s[i];
			newnode->mr[cntm]=s[i];
			newnode->ml[cntm]=s[i];
		}
	}
	newnode->numr=cntm;
	newnode->numl=cntm;
	qsort(newnode->mr,cntm+1,sizeof(PAIR),compmr);
	qsort(newnode->ml,cntm+1,sizeof(PAIR),compml);
	newnode->left=intTreeNode(l,0,cntl);
	newnode->right=intTreeNode(r,0,cntr);
	return newnode;
}
void stab(TREE* t,int xq)
{
	int i;
	if (t==NULL)return;
	if(xq<t->xmed)
	{
		for(i=0;i<=t->numl;i++)
		{
			if(((t->ml[i]).xlo)<=xq)
			{
				printf ("%d %d\n",t->ml[i].xlo,t->ml[i].xhi);
			}else{
				break;
			}
		}
			stab(t->left,xq);
	}else{
		for(i=0;i<=t->numr;i++)
		{
			if(t->mr[i].xhi>=xq)
			{
				printf ("%d %d\n",t->mr[i].xlo,t->mr[i].xhi);
			}else{
				break;
			}
		}
		stab(t->right,xq);
	}
}
int main()
{
	int i,j,t,n;
	PAIR s[100];
	scanf("%d",&n);
	TREE*root;
	for(i=0;i<n;i++)
	{
		scanf("%d %d",&s[i].xlo,&s[i].xhi);
	}
	root=intTreeNode(s,0,n-1);
	printf("enter queries: ");
	int cmd;
	while(1)
	{
		printf("enter queries: ");
		scanf("%d",&cmd);
		if(cmd<0)
			break;
		stab(root,cmd);
	}
	//while(t--)
	return 0;
}

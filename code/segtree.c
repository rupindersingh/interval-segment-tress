#include<stdio.h>
#include<stdlib.h>
typedef struct node
{
    int max;
    int gcd;
    int sum;
}node;
node tree[1<<20];
int ql,qu,base;
#define MAX_IDENTITY 0
#define GCD_IDENTITY 0
#define SUM_IDENTITY 0
#define MODBY 1000000009
void buildtree(int n);
  void  build_max(int n);
  void  build_gcd(int n);
  void  build_sum(int n);
void update(int idx,int b);

int loga2(const int n)
{
    int j=n,ct=0,ct2=0;
    while(j)
    {
	if(!(j&1))
	    ct2++;
	j/=2;
	ct++;
    }
    return (ct2==ct-1)?ct-1:ct;																							
}
int maximum(int a,int b)
{
    return (a>b)?a:b;
}
int hcf(int a,int b)
{
    return (a)?hcf(b%a,a):b;
}

int main()
{
    int n,a,b,k,i,ans;
    char ch[20];
    scanf("%d",&n);//No. of elements
    k=loga2(n);
    base=(1<<k);
    buildtree(n);
    scanf("%s",ch);
    while(strcmp(ch,"Quit"))
    {
	if(!strcmp(ch,"Update"))
	{
	    scanf("%d%d",&a,&b);
	    update(a-1,b);
	}
	else if(!strcmp(ch,"Max"))
	{
	    scanf("%d%d",&a,&b);
            if(b<a)
            puts("Invalid Range");
            else
            {
	    ql=a-1;
	    qu=b-1;
	    ans=query_max(1,0,base-1);
	    printf("%d\n",ans);
            }
	}
	else if(!strcmp(ch,"Gcd"))
	{
	    scanf("%d%d",&a,&b);
            if(b<a)
            puts("Invalid Range");
            else
            {
	    ql=a-1;
	    qu=b-1;
	    ans=query_gcd(1,0,base-1);
	    printf("%d\n",ans);
            }
	}
	else if(!strcmp(ch,"Sum"))
	{
	    scanf("%d%d",&a,&b);
            if(b<a)
            puts("Invalid Range");
            else
            {

	    query_sum(a-1,b-1);
	    ql=a-1;
	    qu=b-1;
	    ans=query_sum(1,0,base-1);
	    printf("%d\n",ans);
            }
	}
	else
	    puts("Invalid request");
	scanf("%s",ch);
    }


}
void buildtree(int n)
{
    int i,j,k,pos;
    //Building the seg tree
    for(i=0;i<n;i++)
    {
	scanf("%d",&k);
	pos=base+i;
	tree[pos].max=k;
	tree[pos].gcd=k;
	tree[pos].sum=k;
    }
    //Fill the tree till an exact power of 2
    for(j=i;j<base;j++)
    {
	pos=base+j;
	tree[pos].max=MAX_IDENTITY;
	tree[pos].gcd=GCD_IDENTITY;
	tree[pos].sum=SUM_IDENTITY;
    }
    build_max(n);
    build_gcd(n);
    build_sum(n);

}
void build_max(int n)
{
    int i;
    int curr=base/2;
    while(curr)
    {
	for(i=curr;i<=2*curr-1;i++)
	    tree[i].max=maximum(tree[i*2].max,tree[i*2+1].max);
	curr/=2;
    }
}

void build_gcd(int n)
{
    int i;
    int curr=base/2;
    while(curr)
    {
	for(i=curr;i<=2*curr-1;i++)
	    tree[i].gcd=hcf(tree[i*2].gcd,tree[i*2+1].gcd);
	curr/=2;
    }
}

void build_sum(int n)
{
    int curr=base/2,i;
    long long int ans;
    while(curr)
    {
	for(i=curr;i<=2*curr-1;i++)
	{
		ans=tree[i*2].sum+tree[i*2+1].sum;
	        tree[i].sum=ans%MODBY;
	}
	curr/=2;
    }
}

void update(int idx,int b)
{
    long long int ans;
    int o=base+idx;
    tree[o].max=tree[o].gcd=tree[o].sum=b;
    int curr=o;
    int p=(o&1)?-1:1;
    while(curr)
    {
	tree[curr/2].max=maximum(tree[curr].max,tree[curr+p].max);
	curr=curr/2;
    }
    curr=o;
    while(curr)
    {
	tree[curr/2].gcd=hcf(tree[curr].gcd,tree[curr+p].gcd);
	curr=curr/2;
    }
    curr=o;
    while(curr)
    {
        ans=tree[curr].sum+tree[curr+p].sum;
	tree[curr/2].sum=ans%MODBY;
	curr=curr/2;
    }
}
int query_max(int idx,int cl,int cu)
{
    if(ql>cu || qu<cl)
	return 0;
    if(cl>=ql && cu<=qu)
	return tree[idx].max;
    else
	return maximum(query_max(idx*2,cl,(cl+cu)/2),query_max(idx*2+1,(cl+cu)/2+1,cu));
}

int query_gcd(int idx,int cl,int cu)
{
    if(ql>cu || qu<cl)
	return 0;
    if(cl>=ql && cu<=qu)
	return tree[idx].gcd;
    else
	return hcf(query_gcd(idx*2,cl,(cl+cu)/2),query_gcd(idx*2+1,(cl+cu)/2+1,cu));
}

int query_sum(int idx,int cl,int cu)
{
    long long ans;
    if(ql>cu || qu<cl)
	return 0;
    if(cl>=ql && cu<=qu)
	return tree[idx].sum;
    else
    {
	ans=query_sum(idx*2,cl,(cl+cu)/2)+query_sum(idx*2+1,(cl+cu)/2+1,cu);
	return ans%MODBY;
    }
}


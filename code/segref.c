//Update
            /*1-based Index a to b
	    scanf("%d%d",&a,&b);
	    update(a-1,b);*/
           

//   Query
            /*
	    scanf("%d%d",&a,&b);
            
	    ql=a-1;
	    qu=b-1;
	    ans=query_max(1,0,base-1);
	    printf("%d\n",ans);
           */
	
#include<stdio.h>
#include<stdlib.h>
int tree[1<<20];
int ql,qu,base;
#define MAX_IDENTITY 0
#define GCD_IDENTITY 0
#define SUM_IDENTITY 0
#define MODBY 1000000009
void buildtree(int n);
  void  build_max(int n);
  //void  build_gcd(int n);
  //void  build_sum(int n);
void update(int idx,int b);

int loga2(const int n)
{
    int j=n,ct=0,ct2=0;
    while(j)
    {
	if(!(j&1))
	    ct2++;
	j/=2;
	ct++;
    }
    return (ct2==ct-1)?ct-1:ct;																							
}
int maximum(int a,int b)
{
    return (a>b)?a:b;
}
int hcf(int a,int b)
{
    return (a)?hcf(b%a,a):b;
}

int main()
{
    int n,a,b,k,i,ans;
   
    scanf("%d",&n);//No. of elements
    k=loga2(n);
    base=(1<<k);
    buildtree(n);
   

return 0;
}
//Input is taken in this
void buildtree(int n)
{
    int i,j,k,pos;
    //Building the seg tree
    for(i=0;i<n;i++)
    {
	scanf("%d",&k);
	pos=base+i;
	tree[pos]=k;
	
    }
    //Fill the tree till an exact power of 2
    for(j=i;j<base;j++)
    {
	pos=base+j;
	tree[pos]=MAX_IDENTITY;
	
    }
    build_max(n);
    
}
void build_max(int n)
{
    int i;
    int curr=base/2;
    while(curr)
    {
	for(i=curr;i<=2*curr-1;i++)
	    tree[i]=maximum(tree[i*2],tree[i*2+1]);
	curr/=2;
    }
}



void update(int idx,int b)
{
    long long int ans;
    int o=base+idx;
    tree[o]=b;
    int curr=o;
    //int p=(o&1)?-1:1;
    while(curr)
    {
        int p=(o&1)?-1:1;
	tree[curr/2]=maximum(tree[curr],tree[curr+p]);
	curr=curr/2;
    }
}
    
int query_max(int idx,int cl,int cu)
{
    if(ql>cu || qu<cl)
	return 0;
    if(cl>=ql && cu<=qu)
	return tree[idx];
    else
	return maximum(query_max(idx*2,cl,(cl+cu)/2),query_max(idx*2+1,(cl+cu)/2+1,cu));
}

/*int query_gcd(int idx,int cl,int cu)
{
    if(ql>cu || qu<cl)
	return 0;
    if(cl>=ql && cu<=qu)
	return tree[idx].gcd;
    else
	return hcf(query_gcd(idx*2,cl,(cl+cu)/2),query_gcd(idx*2+1,(cl+cu)/2+1,cu));
}

int query_sum(int idx,int cl,int cu)
{
    long long ans;
    if(ql>cu || qu<cl)
	return 0;
    if(cl>=ql && cu<=qu)
	return tree[idx].sum;
    else
    {
	ans=query_sum(idx*2,cl,(cl+cu)/2)+query_sum(idx*2+1,(cl+cu)/2+1,cu);
	return ans%MODBY;
    }
}
*/
